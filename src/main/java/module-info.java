module exam {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;


    opens winning_calculator;
    opens controllers;
    opens state;
}